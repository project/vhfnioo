# Views Numeric Multiple Filter

By default the [Views](https://drupal.org/project/views) module provide a [\views_handler_filter_numeric](https://api.drupal.org/api/views/handlers!views_handler_filter_numeric.inc/class/views_handler_filter_numeric/7) object for filtering query results which is unable to filter using multiple values. This module fix it up. 

# Usage

- Enable the module
- Go to filter editing
- Choose one of `is one of` or `is not one of` filters
